const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const courseController = require("../controllers/courseController.js")

/* 
Activity:
1. Refactor the course route to implement user authentication for the admin when creating a course.
2. Refactor the addCourse controller method to implement admin authentication for creating a course.
3. Create a git repository named S34.
4. Add another remote link and push to git with the commit message of Add activity code.
5. Add the link in Boodle.

*/

/* 
router.post("/addcourse", auth.verify, (req, res)=> {

	const userData = auth.decode(req.headers.authorization)

    console.log(userData)

	courseController.addCourse( req.body, userData ).then(result => res.send(result))
} ) */




/*
ACTIVITY
create a route that will let an admin perform addCourse function in the courseController
	verify that the user is logged in
	decode the token for that user
	use the id, isAdmin, and request body to perform the function in courseController
		the id and isAdmin are parts of an object 
*/
router.post("/", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization) //userData would now contain an object that has the token payload (id, email, isAdmin information of the user)
    courseController.addCourse(req.body, userData).then(resultFromController => res.send(resultFromController))
})


/*	ecommerce websites */

/*
create a route that will retrieve all of our products/courses
	will not require login/register functions
*/
router.get("/", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

/*
	in getting all of the documents, in case we need multiple of them, place the route with the criteria to the find method first before getting the one with params
*/
// retrieve all active courses
router.get("/active", (req, res) => {
	courseController.getActive().then(resultFromController => res.send(resultFromController))
})

/*
miniactivty
	create a route that will retrieve a course hint: params
		will not require login/register from the users
*/
// retrieve a course
router.get("/:courseId",  (req, res) => {
	console.log(req.params.courseId);
	courseController.getCourse(req.params.courseId).then(result => res.send(result))
})

// update a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(result => res.send(result))
})

/*
delete is never a norm in databases

use /archiveCourse and send a PUT request to archive a course by changing the active status
*/

router.put("/:courseID/archiveCourse", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(result => res.send(result))
})

module.exports = router