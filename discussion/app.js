const express = require ("express")
const mongoose = require ("mongoose")
const cors = require ("cors")

// access to routes
const courseRoutes = require ("./routes/courseRoutes.js")
const userRoutes = require ("./routes/userRoutes.js")

// server
const app = express()
const port = 4000

app.use(cors()) //allows all origins/domains to access backend application

app.use(express.json()) //so we can read json files
app.use(express.urlencoded({extended:true}))



mongoose.connect("mongodb+srv://verunque_david:password1210@wdc028-course-booking.13hym.mongodb.net/b170-to-do?retryWrites=true&w=majority", {
    useNewUrlParser: true,
	useUnifiedTopology:true
})



// defines the routes where the CRUD operations will be executed on the users ("/api/users") and courses ("/api/courses")
//main endpoint
app.use("/api/users", userRoutes);
app.use("/api/courses", courseRoutes);



let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the database"))

app.listen(port, () => console.log(`API now online at port ${port}`))