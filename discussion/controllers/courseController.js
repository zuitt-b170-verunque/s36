/* 
Activity:
1. Refactor the course route to implement user authentication for the admin when creating a course.
2. Refactor the addCourse controller method to implement admin authentication for creating a course.
3. Create a git repository named S34.
4. Add another remote link and push to git with the commit message of Add activity code.
5. Add the link in Boodle.

*/

const User = require("../models/User.js")
const Course = require("../models/Course.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")
const { request } = require("express")




// if isAdmin === true then addCourse

 
module.exports.addCourse = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin === false) {
            return "You are not an admin"
        } else {
            let newCourse = new Course ({
                name : requestBody.name,
                description : requestBody.description,
                price : requestBody.price,
                isActive : requestBody.isActive,
                createdOn : requestBody.createdOn,
            })
            return newCourse.save().then((saved, error)=>{
                if (error){
                    console.log(error)
                    return false
                } 
                else {
                    return true
                }
            })
        }
        
    });    
}


// retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find( {} ).then((result, error) => {
		if (error) {
			return false
		}else{
			return result
		}
	})
}

/*
	using the course id (params) in the url, retrieve a course using a get request
		send the code snippet in the batch google chat
*/

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result
	})
}

// retrieve all active courses
module.exports.getActive = () => {
	return Course.find( { isActive: true } ).then((result,error) => {
		if (error) {
			console.log(error)
		}else{
			return result
		}
	})
}


// archive a course
// update a course
module.exports.updateCourse = ( reqParams, reqBody ) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		isActive : reqBody.isActive
	}
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
	})
}

/*
	1. create a updateCourse object with the content from the reqBody
			reqBody should have the isActive status of the course to be set to false
	2. find the course by its id and update with the updateCourse object using the findByIdAndUpdate
			handle the errors that may arise
				error/s - false
				no errors - true
*/

module.exports.archiveCourse = ( reqParams, reqBody ) => {
	let updatedCourse = {
		isActive : reqBody.isActive
	}
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
	})
}



