// dependency

const User = require("../models/User.js")
const Course = require("../models/Course.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt") //used to encrypt user passwords
const { findOne } = require("../models/User.js")

module.exports.checkEmail = (requestBody) => {
	return User.find({ email: requestBody.email }).then((result, error) => {
		if (error) {
			console.log(error)
			return false
		}else{
			if (result.length > 0) {
				// return result
				return true
			} else {
				// return res.send("email does not exist")
                return false
			}
		}
	})
}

// USER REGISTRATIONS
/* 
1. create new User with the information from the requestBody
2. make sure that the password is encrypted
3. save the new user 
*/

module.exports.registerUser = (reqBody) => {
    let newUser = new User ({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        age : reqBody.age,
        gender : reqBody.gender,
        email : reqBody.email,
        // haschSycnch is a function of bcrypt that encrypts the password
            // 10 the number of rounds/times it runs the algorithm to the reqBody.password
                //max 72 implementation of algorithm
        password : bcrypt.hashSync(reqBody.password, 10),
        mobileNo : reqBody.mobileNo,
    })
    return newUser.save().then((saved, error)=>{
        if (error){
            console.log(error)
            return false
        } else {
            return true
        }
    })
}

// USER LOGIN
/* 
1. find if the email exists in the database
2. check if the password is correct
*/

module.exports.userLogin = (reqBody) => {
    return User.findOne( {email:reqBody.email}).then( result => {
        if (result === null) {
            return false
        } else {
            // compareSync function - used to compare a non-encrypted password to an enecrypted password and returns a Boolean response depending on the result
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if (isPasswordCorrect) {
                // prints/returns an access token
                return {access: auth.createAccessToken(result.toObject())}
            } else {
                return false
            }
        }
    })
}

// MINI ACTIVITY
/* module.exports.getProfile = (data) => {
    return User.findById(data.userId).then(result => {
        console.log(result)

        if (result === null) {
            return false
        } 

        else {
            result.password = "" //this is what the user will see, this is just a placeholder
            return result
        }


    })
}
 */
module.exports.getProfile = (data) => {
	return User.findById( data.userId ).then(result =>{
        console.log(`user ID ${data.isAdmin}`)
		if (result === null) {
			return false
		} else {
			result.password = "";
			return result
		}
	})
}



// ENROLLMENT
/* 
1. find the user / document in the database
2. add the courseId to the user's enrolment array
3. save the document in the database
*/


module.exports.enroll = async (data) => {
    let isUserUpdated = await User.findById(data.userId).then(user => {
        user.enrollments.push({courseId:data.courseId})
        
        // saving
        return user.save().then((user, err) => {
        if (err) {
            return false
        } else {
                return true
            }
        })
    })
    /* 
    use another await keyword to update the enrollees array in the course collection
    
                find the courseId from the requestBody
                push the userId of the enrollee in the enrollees array of the course
                update the document in the database

                if both pushing are successful, return true
                if both pushing are not successful, return false
    */

    let isCourseUpdated = await Course.findById(data.courseId).then(course => {
        course.enrollees.push({userId:data.userId})

        return course.save().then((user, err) => {
            if (err) {
                return false
            } else {
                    return true
                }
            })
    })

    if (isUserUpdated){
        return true
    } else {
        return false
    }
}
